(function ( $ ) {
    $.fn.extend({
        animateCss: function (animationName) {
            var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
            $(this).addClass('animated ' + animationName).one(animationEnd, function () {
                $(this).removeClass('animated ' + animationName);
            });
        }
    });

    $('.front').animateCss('fadeInLeft');
    $('img.center').animateCss('bounceInDown');
    $('.footer-left').animateCss('fadeInLeft');
    $('.footer-middle').animateCss('fadeInUp');
    $('.footer-right').animateCss('fadeInRight');

}(jQuery));