<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div class="row bg-holder">
	<img class="center" src="<?php echo get_stylesheet_directory_uri(); ?>/images/kold-katcher-logo.png" />

	<nav id="site-navigation" class="main-navigation navbar navbar-default front" role="navigation">
		<div class="container">
		<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'koldkatcher' ); ?></a>
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-main">
				<span class="sr-only"><?php _e('Toggle navigation', 'koldkatcher'); ?></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<!--<a class="navbar-brand" href="#">Brand</a>-->
		</div>

		<div class="collapse navbar-collapse" id="navbar-collapse-main">
			<ul class="nav navbar-nav">
				<?php if( has_nav_menu( 'primary' ) ) :
					wp_nav_menu( array(
							'theme_location'  => 'primary',
							'container'       => false,
							//'menu_class'      => 'nav navbar-nav',//  'nav navbar-right'
							'walker'          => new Bootstrap_Nav_Menu(),
							'fallback_cb'     => null,
							'items_wrap'      => '%3$s',// skip the containing <ul>
						)
					);
				else :
					wp_list_pages( array(
							'menu_class'      => 'nav navbar-nav',//  'nav navbar-right'
							'walker'          => new Bootstrap_Page_Menu(),
							'title_li'        => null,
						)
					);
				endif; ?>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div>
	</nav><!-- #site-navigation -->

	<div class="clr"></div>
</div>

<?php wp_footer(); ?>
<?php get_footer(); ?>

</body>