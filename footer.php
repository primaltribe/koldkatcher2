</div>
<div class="row no-margin">
	<footer>
		<div class="container tall">
			<?php

			$args = array( 'post_type' => 'footer-item', 'posts_per_page' => 10 );
			$loop = new WP_Query( $args );
			$counter = 0;
			while ( $loop->have_posts() ) : $loop->the_post();

				switch ($counter) {
					case 0:
						echo '<div class="footer-left">';
						echo '<p class="header">';
						the_title();
						echo '</p>';
						the_content();
						echo '</div>';
						$counter++;
						break;
					case 1:
						echo '<div class="footer-middle">';
						echo '<p class="header">';
						the_title();
						echo '</p>';
						the_content();
						echo '</div>';
						$counter++;
						break;
					case 2:
						echo '<div class="footer-right wow animated fadeInRight">';
						the_content();
						echo '</div>';
						$counter++;
						break;
					default:
						break;
				}
			endwhile;
			?>
		</div>

		<div class="footer-full">
			<div class="container">
				<p>Copyright Kold Katcher Incorporated</p>&nbsp;&nbsp;<span>&nbsp;</span>&nbsp;&nbsp;<a href="#">Terms & Conditions</a>&nbsp;&nbsp;<span>&nbsp;</span>&nbsp;&nbsp;<a href="http://primaltribe.com" target="_blank">Website Design by Primal Tribe</a><a href="https://www.youtube.com/channel/UCNUenRMKkXG5hcRkcaZ1aZA" target="_blank" class="youtube"></a>
			</div>
		</div>
	</footer>
</div>

<script>
	new WOW().init();
</script>

<?php wp_footer(); ?>

<script type='text/javascript' src='<?php echo get_stylesheet_directory_uri() ?>/js/scripts.js'></script>
</body>